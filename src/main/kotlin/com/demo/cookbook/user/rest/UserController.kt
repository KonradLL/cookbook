package com.demo.cookbook.user.rest

import com.demo.cookbook.domain.User
import com.demo.cookbook.user.UserService
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/user")
class UserController(
		private val userService: UserService
) {

	@PostMapping("/new")
	fun createUser(@Validated @RequestBody createUserRequest: CreateUserRequest): User {
		return userService.createUser(createUserRequest)
	}
}