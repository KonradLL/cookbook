package com.demo.cookbook.user.rest

data class CreateUserRequest(val name: String)