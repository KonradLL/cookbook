package com.demo.cookbook.user

import com.demo.cookbook.domain.User
import com.demo.cookbook.user.rest.CreateUserRequest
import mu.KotlinLogging
import org.springframework.stereotype.Service

@Service
class UserService(
		private val userRepository: UserRepository
) {

	private val logger = KotlinLogging.logger {}

	fun createUser(createUserRequest: CreateUserRequest): User {
		logger.info { "creating user ${createUserRequest.name}" }
		return userRepository.save(UserDto(username = createUserRequest.name)).mapToDomain()
	}
}