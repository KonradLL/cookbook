package com.demo.cookbook.user

import com.demo.cookbook.domain.User
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table
import org.hibernate.annotations.UuidGenerator
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserRepository : CrudRepository<UserDto, String>

@Entity
@Table(name = "account")
data class UserDto(
		@Id
		@UuidGenerator
		var id: UUID? = null,
		var username: String
) {
	fun mapToDomain(): User {
		return User(
				id = this.id!!,
				username = this.username
		)
	}
}

