package com.demo.cookbook.helloworld

import mu.KotlinLogging
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/hello")
class HelloWorldController(
	val helloWorldService: HelloWorldService
) {

	private val logger = KotlinLogging.logger {}

	@GetMapping("/world")
	fun helloWorld(): String {
		logger.info { "GET hello world request received" }
		return helloWorldService.getHelloWorld();
	}
}