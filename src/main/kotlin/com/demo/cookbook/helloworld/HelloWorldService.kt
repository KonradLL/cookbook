package com.demo.cookbook.helloworld

import mu.KotlinLogging
import org.springframework.stereotype.Service

@Service
class HelloWorldService {

	private val logger = KotlinLogging.logger {}

	fun getHelloWorld(): String {
		logger.info { "Creating hello world" }
		return "hello world";
	}

}