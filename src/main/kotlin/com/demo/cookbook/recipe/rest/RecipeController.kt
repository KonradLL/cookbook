package com.demo.cookbook.recipe.rest

import com.demo.cookbook.domain.Recipe
import com.demo.cookbook.domain.RecipeDifficulty
import com.demo.cookbook.recipe.RecipeService
import org.springframework.stereotype.Controller
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/recipe")
class RecipeController(
        val recipeService: RecipeService
) {

    @GetMapping
    fun getRecipes(@RequestHeader("x-user-id") userId: String): List<Recipe> {
        return recipeService.getUserRecipes(userId)
    }

    @PostMapping
    fun createRecipe(@Validated @RequestBody request: CreateRecipeRequest): Recipe {
        return recipeService.createRecipe(request)
    }

}

