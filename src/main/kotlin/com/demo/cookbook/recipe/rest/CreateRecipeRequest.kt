package com.demo.cookbook.recipe.rest

import com.demo.cookbook.domain.RecipeDifficulty

data class CreateRecipeRequest(
        val name: String,
        val difficulty: RecipeDifficulty,
        val durationMinutes: Int,
)