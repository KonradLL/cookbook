package com.demo.cookbook.recipe

import com.demo.cookbook.domain.Recipe
import com.demo.cookbook.domain.RecipeDifficulty
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table
import org.hibernate.annotations.UuidGenerator
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface RecipeRepository : CrudRepository<RecipeDto, String> {
	fun findAllByAccount(userId: String): List<RecipeDto>
}

@Entity
@Table(name = "recipe")
data class RecipeDto(
		@Id
		@UuidGenerator
        var id: UUID? = null,
		var account: UUID,
		var name: String,
		var difficulty: RecipeDifficulty,
		var durationMinutes: Int,
		var cuisine: String,
		var servingSize: String,
) {
	fun mapToDomain(): Recipe {
        return Recipe(
				id = this.id!!,
				account = this.account,
				name = this.name,
				difficulty = this.difficulty,
				durationMinutes = this.durationMinutes,
				cuisine = this.cuisine,
				servingSize = this.servingSize)
}
}

