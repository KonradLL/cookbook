package com.demo.cookbook.recipe

import com.demo.cookbook.domain.Recipe
import com.demo.cookbook.recipe.rest.CreateRecipeRequest
import org.springframework.stereotype.Service
import java.util.*

@Service
class RecipeService(
        val recipeRepository: RecipeRepository
) {

    fun createRecipe(newRecipeRequest: CreateRecipeRequest): Recipe {
        val newRecipe = mapNewRecipe(newRecipeRequest)
        return recipeRepository.save(newRecipe).mapToDomain()
    }

    fun getUserRecipes(userId: String): List<Recipe> {
        return recipeRepository.findAllByAccount(userId).map { recipeDto -> recipeDto.mapToDomain() }
    }

    private fun mapNewRecipe(newRecipe: CreateRecipeRequest): RecipeDto {
        return RecipeDto(
                account = UUID.randomUUID(),
                name = newRecipe.name,
                difficulty = newRecipe.difficulty,
                durationMinutes = newRecipe.durationMinutes,
                cuisine = "",
                servingSize = ""
        )
    }
}