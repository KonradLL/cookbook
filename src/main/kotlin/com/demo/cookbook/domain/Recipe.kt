package com.demo.cookbook.domain

import java.util.*

data class Recipe(
        val id: UUID,
        val account: UUID,
        val name: String,
        val difficulty: RecipeDifficulty,
        val durationMinutes: Int,
        val cuisine: String,
        val servingSize: String,
//        val ingredients  //todo
//        val methods      //todo
)

enum class RecipeDifficulty {
    BEGINNER, INTERMEDIARY, EXPERT
}


