package com.demo.cookbook.domain

import java.util.*

data class User(
	val id: UUID,
	val username: String
)