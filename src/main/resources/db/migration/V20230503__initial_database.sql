create table if not exists account(
    id uuid primary key,
    username varchar(50) not null
);

create table if not exists recipe(
    id uuid  primary key,
    account uuid not null,
    name varchar(64) not null,
    difficulty varchar(32),
    duration_minutes int,
    cuisine varchar(64),
    servingSize varchar(64)
);
