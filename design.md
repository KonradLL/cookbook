# Design


## Model
- User
  - id
  - name


- Recipe
  + id
  - userId
  - name
  - picture
  - difficulty
  - cuisine
  - duration
  - serving size
  - ingredients


- cooking method step
  + recipe fk
  + step nr
  - description


## Features

### MVP
- Create account
- Create recipe
- Update recipe
- List your recipes

### Later
- Searching recipes
- Identity provider
- Observability